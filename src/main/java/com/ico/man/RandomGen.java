package com.ico.man;

import java.util.Arrays;
import java.util.Random;

public class RandomGen {

    private static final double PRECISION = 0.001;

    private Random rand = new Random();

    // Values that may be returned by nextNum() private
    private int[] randomNums;

    // Probability of the occurence of randomNums private
    private float[] probabilities;

    private double[] cumulativeProbabilities;

    public RandomGen(final int[] items, final float[] probabilities) {
        this.randomNums = items;
        this.probabilities = probabilities;
        this.cumulativeProbabilities = new double[items.length];

        init();
    }

    public void init() {
        if(randomNums.length != probabilities.length) {
            throw new IllegalStateException("Probabilities and randomNums should be same size.");
        }
        double cumulativeProbability = 0.0;

        for (int i = 0; i < randomNums.length; i++) {
            cumulativeProbability += probabilities[i];
            this.cumulativeProbabilities[i] = cumulativeProbability;
        }

        if (Math.abs(cumulativeProbability - 1.0) > PRECISION) {
            throw new IllegalStateException("Probabilities do not sum to 1.0.");
        } else {
            this.cumulativeProbabilities[randomNums.length - 1] = 1.0;
        }
    }

    public void setSeed(long seed){
        rand.setSeed(seed);
    }

    /**
    Returns one of the randomNums. When this method is called
    multiple times over a long period, it should return the
    numbers roughly with the initialized probabilities.
    */
    public int nextNum() {

        double choice = rand.nextDouble();

        // equal to (-(i)-1) where cumulativeProbabilities[i] is the first element > choice
        int searchResult = Arrays.binarySearch(this.cumulativeProbabilities, choice);
        int i = -(searchResult + 1);

        return randomNums[i];
    }
}