package com.ico.man;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.withPrecision;
import static org.assertj.core.api.Assertions.within;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RandomGenTest {

    private RandomGen randomGen;

    @Test
    void nextNum_happyFlow_bigSize() {
        int[] randomNums = {-1, 0, 1, 2, 3};
        float[] probabilities = {0.01f, 0.3f, 0.58f, 0.1f, 0.01f};
        randomGen = new RandomGen(randomNums, probabilities);

        int[] extracted = new int[randomNums.length];
        float[] percentages = new float[randomNums.length];

        int allNumbers = 10000000;
        for(int i = 0; i < allNumbers; i++) {
            int num = randomGen.nextNum();
            extracted[num + 1] += 1;
        }
        for(int i = 0; i < extracted.length; i++) {
            //System.out.println(i - 1 + " : " + extracted[i]);
            percentages[i] = (float) extracted[i]/allNumbers;
        }

        assertThat(percentages).hasSameSizeAs(probabilities);
        for(int i = 0; i < probabilities.length; i++) {
            assertThat(percentages[i]).isEqualTo(probabilities[i], within(0.002f));
        }
    }

    @Test
    void nextNum_happyFlow_smallSize() {
        int[] randomNums = {-1, 0, 1, 2, 3};
        float[] probabilities = {0.01f, 0.3f, 0.58f, 0.1f, 0.01f};
        randomGen = new RandomGen(randomNums, probabilities);

        int[] extracted = new int[randomNums.length];
        float[] percentages = new float[randomNums.length];

        int allNumbers = 1000;
        for(int i = 0; i < allNumbers; i++) {
            int num = randomGen.nextNum();
            extracted[num + 1] += 1;
        }
        for(int i = 0; i < extracted.length; i++) {
            //System.out.println(i - 1 + " : " + extracted[i]);
            percentages[i] = (float) extracted[i]/allNumbers;
        }

        assertThat(percentages).hasSameSizeAs(probabilities);
        for(int i = 0; i < probabilities.length; i++) {
            assertThat(percentages[i]).isEqualTo(probabilities[i], within(0.03f));
        }
    }

    //It's failing from time to time because 100 is really small size of generated
    @Test
    void nextNum_happyFlow_100() {
        int[] randomNums = {-1, 0, 1, 2, 3};
        float[] probabilities = {0.01f, 0.3f, 0.58f, 0.1f, 0.01f};
        randomGen = new RandomGen(randomNums, probabilities);

        int[] extracted = new int[randomNums.length];
        float[] percentages = new float[randomNums.length];

        int allNumbers = 100;
        for(int i = 0; i < allNumbers; i++) {
            int num = randomGen.nextNum();
            extracted[num + 1] += 1;
        }

        for(int i = 0; i < extracted.length; i++) {
            percentages[i] = (float) extracted[i]/allNumbers;
        }

        assertThat(percentages).hasSameSizeAs(probabilities);
        for(int i = 0; i < probabilities.length; i++) {
            assertThat(percentages[i]).isEqualTo(probabilities[i], within(0.08f));
        }
    }

    /**
     * It's passing every time and the random numbers are similar because we use a seeded random.
     */
    @Test
    void nextNum_happyFlow_100_seeded() {
        int[] randomNums = {-1, 0, 1, 2, 3};
        float[] probabilities = {0.01f, 0.3f, 0.58f, 0.1f, 0.01f};
        randomGen = new RandomGen(randomNums, probabilities);
        randomGen.setSeed(331);

        int[] extracted = new int[randomNums.length];
        float[] percentages = new float[randomNums.length];

        int allNumbers = 100;
        for(int i = 0; i < allNumbers; i++) {
            int num = randomGen.nextNum();
            extracted[num + 1] += 1;
        }
        for(int i = 0; i < extracted.length; i++) {
            //System.out.println(i - 1 + " : " + extracted[i]);
            percentages[i] = (float) extracted[i]/allNumbers;
        }
        int[] expected = {1, 34, 56, 8, 1};
        assertThat(extracted).isEqualTo(expected);
    }

    @Test
    void nextNum_happyFlow_10_seeded() {
        int[] randomNums = {-1, 0, 1, 2, 3};
        float[] probabilities = {0.01f, 0.3f, 0.58f, 0.1f, 0.01f};
        randomGen = new RandomGen(randomNums, probabilities);
        randomGen.setSeed(24);

        int[] extracted = new int[randomNums.length];
        float[] percentages = new float[randomNums.length];

        int allNumbers = 10;
        int[] results = new int[allNumbers];
        for(int i = 0; i < allNumbers; i++) {
            results[i] = randomGen.nextNum();
        }
        int[] expected = {1, 1, 0, 0, 1, 2, 1, 1, 1, 2};
        assertThat(results).isEqualTo(expected);
    }

    @Test
    void nextNum_differentSizeProbabilities() {
        int[] randomNums = {-1, 0, 1, 2, 3};
        float[] probabilities = {0.01f, 0.3f, 0.58f, 0.1f};
        //randomGen = new RandomGen(randomNums, probabilities);

        assertThatThrownBy(() -> new RandomGen(randomNums, probabilities))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Probabilities and randomNums should be same size.");
    }

    @Test
    void nextNum_probabilitiesDifferentThanOne() {
        int[] randomNums = {-1, 0, 1, 2, 3};
        float[] probabilities = {0.01f, 0.3f, 0f, 0.1f, 0f};
        //randomGen = new RandomGen(randomNums, probabilities);

        assertThatThrownBy(() -> new RandomGen(randomNums, probabilities))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Probabilities do not sum to 1.0.");
    }

    @Test
    void nextNum_emptyArray() {
        int[] randomNums = {};
        float[] probabilities = {0.01f, 0.3f, 0.58f, 0.1f, 0.01f};
        //randomGen = new RandomGen(randomNums, probabilities);

        assertThatThrownBy(() -> new RandomGen(randomNums, probabilities))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Probabilities and randomNums should be same size.");
    }
}